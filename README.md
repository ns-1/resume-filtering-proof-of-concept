# Resume Filtering Proof of Concept

Proof of concept project implementing NLP Semantic Search techniques for the task of filtering resumes. Scraped resumes from Cornell University's career services toolkit, and then implemented various semantic search algorithms utilizing SBERT, OpenAI, TFIDF, and LSA. Compared and contrasted these various methods. 
